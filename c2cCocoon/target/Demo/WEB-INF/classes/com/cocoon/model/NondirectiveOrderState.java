package com.cocoon.model;

public class NondirectiveOrderState {
  private Long nondirectiveOrderStateId;
  private String nondirectiveOrderStateName;
  private String nondirectiveOrderStateDesc;

  public Long getNondirectiveOrderStateId() {
    return nondirectiveOrderStateId;
  }

  public void setNondirectiveOrderStateId(Long nondirectiveOrderStateId) {
    this.nondirectiveOrderStateId = nondirectiveOrderStateId;
  }

  public String getNondirectiveOrderStateName() {
    return nondirectiveOrderStateName;
  }

  public void setNondirectiveOrderStateName(String nondirectiveOrderStateName) {
    this.nondirectiveOrderStateName = nondirectiveOrderStateName;
  }

  public String getNondirectiveOrderStateDesc() {
    return nondirectiveOrderStateDesc;
  }

  public void setNondirectiveOrderStateDesc(String nondirectiveOrderStateDesc) {
    this.nondirectiveOrderStateDesc = nondirectiveOrderStateDesc;
  }
}
