package com.cocoon.model;

import java.sql.Timestamp;

public class DirectiveOrder {
  private Long directiveOrderId;
  private Long lodgerId;
  private Long houseId;
  private Long directiveOrderStateId;
  private java.sql.Timestamp directiveOrderBeginTime;
  private java.sql.Timestamp directiveOrderDurationTime;

  public Long getDirectiveOrderId() {
    return directiveOrderId;
  }

  public void setDirectiveOrderId(Long directiveOrderId) {
    this.directiveOrderId = directiveOrderId;
  }

  public Long getLodgerId() {
    return lodgerId;
  }

  public void setLodgerId(Long lodgerId) {
    this.lodgerId = lodgerId;
  }

  public Long getHouseId() {
    return houseId;
  }

  public void setHouseId(Long houseId) {
    this.houseId = houseId;
  }

  public Long getDirectiveOrderStateId() {
    return directiveOrderStateId;
  }

  public void setDirectiveOrderStateId(Long directiveOrderStateId) {
    this.directiveOrderStateId = directiveOrderStateId;
  }

  public Timestamp getDirectiveOrderBeginTime() {
    return directiveOrderBeginTime;
  }

  public void setDirectiveOrderBeginTime(Timestamp directiveOrderBeginTime) {
    this.directiveOrderBeginTime = directiveOrderBeginTime;
  }

  public Timestamp getDirectiveOrderDurationTime() {
    return directiveOrderDurationTime;
  }

  public void setDirectiveOrderDurationTime(Timestamp directiveOrderDurationTime) {
    this.directiveOrderDurationTime = directiveOrderDurationTime;
  }
}
