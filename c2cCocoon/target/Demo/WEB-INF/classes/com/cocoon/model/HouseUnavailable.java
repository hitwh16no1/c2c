package com.cocoon.model;

import java.sql.Timestamp;

public class HouseUnavailable {
  private Long houseId;
  private java.sql.Timestamp houseUnavailableBeginTime;
  private java.sql.Timestamp houseUnavailableEndTime;

  public Long getHouseId() {
    return houseId;
  }

  public void setHouseId(Long houseId) {
    this.houseId = houseId;
  }

  public Timestamp getHouseUnavailableBeginTime() {
    return houseUnavailableBeginTime;
  }

  public void setHouseUnavailableBeginTime(Timestamp houseUnavailableBeginTime) {
    this.houseUnavailableBeginTime = houseUnavailableBeginTime;
  }

  public Timestamp getHouseUnavailableEndTime() {
    return houseUnavailableEndTime;
  }

  public void setHouseUnavailableEndTime(Timestamp houseUnavailableEndTime) {
    this.houseUnavailableEndTime = houseUnavailableEndTime;
  }
}
