package com.cocoon.model;

public class HouseState {
  private Long houseStateId;
  private String houseStateName;

  public Long getHouseStateId() {
    return houseStateId;
  }

  public void setHouseStateId(Long houseStateId) {
    this.houseStateId = houseStateId;
  }

  public String getHouseStateName() {
    return houseStateName;
  }

  public void setHouseStateName(String houseStateName) {
    this.houseStateName = houseStateName;
  }
}
