package com.cocoon.controller;

import com.cocoon.dao.LodgerMapper;
import com.cocoon.model.Lodger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/Hello")
public class HelloController {
    @Autowired
    private LodgerMapper lodgerMapper;

    @RequestMapping("/test")
    public ModelAndView hello() throws Exception{
        ModelAndView modelAndView = new ModelAndView();
        List<Lodger> lodgers = lodgerMapper.queryAllLodgers();
        for (int i = 0; i < lodgers.size(); ++i){
            System.out.println(lodgers.get(i));
        }
        System.out.println("haha");
        modelAndView.setViewName("index");
        return modelAndView;
    }
}
