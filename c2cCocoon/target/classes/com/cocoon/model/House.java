package com.cocoon.model;

public class House {
  private Long houseId;
  private String houseName;
  private String houseAddr;
  private Double houseDailyPrice;
  private Long houseTypeId;
  private String houseDescription;
  private Long houseStateId;
  private Long landlordId;

  public Long getHouseId() {
    return houseId;
  }

  public void setHouseId(Long houseId) {
    this.houseId = houseId;
  }

  public String getHouseName() {
    return houseName;
  }

  public void setHouseName(String houseName) {
    this.houseName = houseName;
  }

  public String getHouseAddr() {
    return houseAddr;
  }

  public void setHouseAddr(String houseAddr) {
    this.houseAddr = houseAddr;
  }

  public Double getHouseDailyPrice() {
    return houseDailyPrice;
  }

  public void setHouseDailyPrice(Double houseDailyPrice) {
    this.houseDailyPrice = houseDailyPrice;
  }

  public Long getHouseTypeId() {
    return houseTypeId;
  }

  public void setHouseTypeId(Long houseTypeId) {
    this.houseTypeId = houseTypeId;
  }

  public String getHouseDescription() {
    return houseDescription;
  }

  public void setHouseDescription(String houseDescription) {
    this.houseDescription = houseDescription;
  }

  public Long getHouseStateId() {
    return houseStateId;
  }

  public void setHouseStateId(Long houseStateId) {
    this.houseStateId = houseStateId;
  }

  public Long getLandlordId() {
    return landlordId;
  }

  public void setLandlordId(Long landlordId) {
    this.landlordId = landlordId;
  }
}
