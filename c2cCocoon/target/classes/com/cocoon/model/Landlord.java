package com.cocoon.model;

import java.sql.Timestamp;

public class Landlord {
  private Long landlordId;
  private String landlordAccount;
  private String landlordPassword;
  private String landlordNickname;
  private String landlordName;
  private java.sql.Timestamp landlordRegisterTime;
  private String landlordPhoneNo;
  private String landlordIdCardNo;
  private String landlordDescription;

  public Long getLandlordId() {
    return landlordId;
  }

  public void setLandlordId(Long landlordId) {
    this.landlordId = landlordId;
  }

  public String getLandlordAccount() {
    return landlordAccount;
  }

  public void setLandlordAccount(String landlordAccount) {
    this.landlordAccount = landlordAccount;
  }

  public String getLandlordPassword() {
    return landlordPassword;
  }

  public void setLandlordPassword(String landlordPassword) {
    this.landlordPassword = landlordPassword;
  }

  public String getLandlordNickname() {
    return landlordNickname;
  }

  public void setLandlordNickname(String landlordNickname) {
    this.landlordNickname = landlordNickname;
  }

  public String getLandlordName() {
    return landlordName;
  }

  public void setLandlordName(String landlordName) {
    this.landlordName = landlordName;
  }

  public Timestamp getLandlordRegisterTime() {
    return landlordRegisterTime;
  }

  public void setLandlordRegisterTime(Timestamp landlordRegisterTime) {
    this.landlordRegisterTime = landlordRegisterTime;
  }

  public String getLandlordPhoneNo() {
    return landlordPhoneNo;
  }

  public void setLandlordPhoneNo(String landlordPhoneNo) {
    this.landlordPhoneNo = landlordPhoneNo;
  }

  public String getLandlordIdCardNo() {
    return landlordIdCardNo;
  }

  public void setLandlordIdCardNo(String landlordIdCardNo) {
    this.landlordIdCardNo = landlordIdCardNo;
  }

  public String getLandlordDescription() {
    return landlordDescription;
  }

  public void setLandlordDescription(String landlordDescription) {
    this.landlordDescription = landlordDescription;
  }
}
