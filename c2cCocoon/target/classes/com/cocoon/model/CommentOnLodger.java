package com.cocoon.model;

public class CommentOnLodger {
  private Long commentOnLodgerId;
  private Long directiveOrderId;
  private String commentInLodgerContent;

  public Long getCommentOnLodgerId() {
    return commentOnLodgerId;
  }

  public void setCommentOnLodgerId(Long commentOnLodgerId) {
    this.commentOnLodgerId = commentOnLodgerId;
  }

  public Long getDirectiveOrderId() {
    return directiveOrderId;
  }

  public void setDirectiveOrderId(Long directiveOrderId) {
    this.directiveOrderId = directiveOrderId;
  }

  public String getCommentInLodgerContent() {
    return commentInLodgerContent;
  }

  public void setCommentInLodgerContent(String commentInLodgerContent) {
    this.commentInLodgerContent = commentInLodgerContent;
  }
}
