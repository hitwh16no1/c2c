package com.cocoon.model;

public class AccommodationInfo {
  private Long tenementId;
  private Long directiveOrderId;

  public Long getTenementId() {
    return tenementId;
  }

  public void setTenementId(Long tenementId) {
    this.tenementId = tenementId;
  }

  public Long getDirectiveOrderId() {
    return directiveOrderId;
  }

  public void setDirectiveOrderId(Long directiveOrderId) {
    this.directiveOrderId = directiveOrderId;
  }
}
