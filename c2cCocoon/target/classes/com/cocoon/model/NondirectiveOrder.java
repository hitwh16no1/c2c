package com.cocoon.model;

import java.sql.Timestamp;

public class NondirectiveOrder {
  private Long nondirectiveOrderId;
  private Long lodgerId;
  private String nondirectiveOrderDesc;
  private Long nondirectiveOrderStateId;
  private java.sql.Timestamp nondirectiveOrderBeginTime;
  private java.sql.Timestamp nondirectiveOrderDurationTime;


  public Long getNondirectiveOrderId() {
    return nondirectiveOrderId;
  }

  public void setNondirectiveOrderId(Long nondirectiveOrderId) {
    this.nondirectiveOrderId = nondirectiveOrderId;
  }

  public Long getLodgerId() {
    return lodgerId;
  }

  public void setLodgerId(Long lodgerId) {
    this.lodgerId = lodgerId;
  }

  public String getNondirectiveOrderDesc() {
    return nondirectiveOrderDesc;
  }

  public void setNondirectiveOrderDesc(String nondirectiveOrderDesc) {
    this.nondirectiveOrderDesc = nondirectiveOrderDesc;
  }

  public Long getNondirectiveOrderStateId() {
    return nondirectiveOrderStateId;
  }

  public void setNondirectiveOrderStateId(Long nondirectiveOrderStateId) {
    this.nondirectiveOrderStateId = nondirectiveOrderStateId;
  }

  public Timestamp getNondirectiveOrderBeginTime() {
    return nondirectiveOrderBeginTime;
  }

  public void setNondirectiveOrderBeginTime(Timestamp nondirectiveOrderBeginTime) {
    this.nondirectiveOrderBeginTime = nondirectiveOrderBeginTime;
  }

  public Timestamp getNondirectiveOrderDurationTime() {
    return nondirectiveOrderDurationTime;
  }

  public void setNondirectiveOrderDurationTime(Timestamp nondirectiveOrderDurationTime) {
    this.nondirectiveOrderDurationTime = nondirectiveOrderDurationTime;
  }
}
