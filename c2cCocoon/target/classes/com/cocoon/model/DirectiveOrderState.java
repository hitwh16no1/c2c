package com.cocoon.model;

public class DirectiveOrderState {
  private Long directiveOrderStateId;
  private String directiveOrderStateName;
  private String directiveOrderStateDesc;


  public Long getDirectiveOrderStateId() {
    return directiveOrderStateId;
  }

  public void setDirectiveOrderStateId(Long directiveOrderStateId) {
    this.directiveOrderStateId = directiveOrderStateId;
  }

  public String getDirectiveOrderStateName() {
    return directiveOrderStateName;
  }

  public void setDirectiveOrderStateName(String directiveOrderStateName) {
    this.directiveOrderStateName = directiveOrderStateName;
  }

  public String getDirectiveOrderStateDesc() {
    return directiveOrderStateDesc;
  }

  public void setDirectiveOrderStateDesc(String directiveOrderStateDesc) {
    this.directiveOrderStateDesc = directiveOrderStateDesc;
  }
}
