package com.cocoon.model;

public class Tenement {
  private Long tenementId;
  private String tenementName;
  private String tenementIdNo;

  public Long getTenementId() {
    return tenementId;
  }

  public void setTenementId(Long tenementId) {
    this.tenementId = tenementId;
  }

  public String getTenementName() {
    return tenementName;
  }

  public void setTenementName(String tenementName) {
    this.tenementName = tenementName;
  }

  public String getTenementIdNo() {
    return tenementIdNo;
  }

  public void setTenementIdNo(String tenementIdNo) {
    this.tenementIdNo = tenementIdNo;
  }
}
