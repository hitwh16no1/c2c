package com.cocoon.model;

public class CommentOnHouse {
  private Long commentOnHouseId;
  private Long directiveOrderId;
  private String commentOnHouseContent;

  public Long getCommentOnHouseId() {
    return commentOnHouseId;
  }

  public void setCommentOnHouseId(Long commentOnHouseId) {
    this.commentOnHouseId = commentOnHouseId;
  }

  public Long getDirectiveOrderId() {
    return directiveOrderId;
  }

  public void setDirectiveOrderId(Long directiveOrderId) {
    this.directiveOrderId = directiveOrderId;
  }

  public String getCommentOnHouseContent() {
    return commentOnHouseContent;
  }

  public void setCommentOnHouseContent(String commentOnHouseContent) {
    this.commentOnHouseContent = commentOnHouseContent;
  }
}
