package com.cocoon.dao;

import com.cocoon.model.NondirectiveOrder;

import java.util.List;

public interface NondirectiveOrderMapper {
    public List<NondirectiveOrder> queryAllNondirectiveOrders() throws Exception;
}
