package com.cocoon.dao;

import com.cocoon.model.House;

import java.util.List;

public interface HouseMapper {
    public List<House> queryAllHouses() throws Exception;
    public List<House> queryAllHousesRecommended() throws Exception;
}
