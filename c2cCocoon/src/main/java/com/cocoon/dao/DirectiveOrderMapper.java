package com.cocoon.dao;

import com.cocoon.model.DirectiveOrder;

import java.util.List;

public interface DirectiveOrderMapper {
    public List<DirectiveOrder> queryAllDirectiveOrders() throws Exception;
}
