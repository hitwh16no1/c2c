package com.cocoon.dao;

import com.cocoon.model.Tenement;

import java.util.List;

public interface TenementMapper {
    public List<Tenement> queryAllTenements() throws Exception;
    public List<Tenement> queryTenementsByDirectiveOrderId(Long directiveOrderId) throws Exception;
}
