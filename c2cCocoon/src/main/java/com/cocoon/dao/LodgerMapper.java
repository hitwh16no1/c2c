package com.cocoon.dao;
import com.cocoon.model.Lodger;

import java.util.List;

public interface LodgerMapper {
    public List<Lodger> queryAllLodgers() throws Exception;
    public List<Lodger> queryPendingLodgers() throws Exception;

}
