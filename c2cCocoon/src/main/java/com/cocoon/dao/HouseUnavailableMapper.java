package com.cocoon.dao;

import com.cocoon.model.HouseUnavailable;

import java.util.List;

public interface HouseUnavailableMapper {
    public List<HouseUnavailable> queryHouseUnavailableInfoByHouseId(Long houseId) throws Exception;

}
