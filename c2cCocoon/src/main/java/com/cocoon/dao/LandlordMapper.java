package com.cocoon.dao;

import com.cocoon.model.Landlord;

import java.util.List;

public interface LandlordMapper {
    public List<Landlord> queryAllLandlords() throws Exception;
    public List<Landlord> queryPendingLandlords() throws Exception;
}
