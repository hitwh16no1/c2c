package com.cocoon.model;

import java.sql.Timestamp;

public class Lodger {
  private Long lodgerId;
  private String lodgerAccount;
  private String lodgerPassword;
  private String lodgerNickname;
  private String lodgerName;
  private java.sql.Timestamp lodgerRegisterTime;
  private String lodgerPhoneNo;
  private String lodgerIdCardNo;
  private String lodgerEmail;
  private String lodgerAddress;
  private Long userStateId;


  public Long getLodgerId() {
    return lodgerId;
  }

  public void setLodgerId(Long lodgerId) {
    this.lodgerId = lodgerId;
  }

  public String getLodgerAccount() {
    return lodgerAccount;
  }

  public void setLodgerAccount(String lodgerAccount) {
    this.lodgerAccount = lodgerAccount;
  }

  public String getLodgerPassword() {
    return lodgerPassword;
  }

  public void setLodgerPassword(String lodgerPassword) {
    this.lodgerPassword = lodgerPassword;
  }

  public String getLodgerNickname() {
    return lodgerNickname;
  }

  public void setLodgerNickname(String lodgerNickname) {
    this.lodgerNickname = lodgerNickname;
  }

  public String getLodgerName() {
    return lodgerName;
  }

  public void setLodgerName(String lodgerName) {
    this.lodgerName = lodgerName;
  }

  public Timestamp getLodgerRegisterTime() {
    return lodgerRegisterTime;
  }

  public void setLodgerRegisterTime(Timestamp lodgerRegisterTime) {
    this.lodgerRegisterTime = lodgerRegisterTime;
  }

  public String getLodgerPhoneNo() {
    return lodgerPhoneNo;
  }

  public void setLodgerPhoneNo(String lodgerPhoneNo) {
    this.lodgerPhoneNo = lodgerPhoneNo;
  }

  public String getLodgerIdCardNo() {
    return lodgerIdCardNo;
  }

  public void setLodgerIdCardNo(String lodgerIdCardNo) {
    this.lodgerIdCardNo = lodgerIdCardNo;
  }

  public String getLodgerEmail() {
    return lodgerEmail;
  }

  public void setLodgerEmail(String lodgerEmail) {
    this.lodgerEmail = lodgerEmail;
  }

  public String getLodgerAddress() {
    return lodgerAddress;
  }

  public void setLodgerAddress(String lodgerAddress) {
    this.lodgerAddress = lodgerAddress;
  }

  @Override
  public String toString() {
    return "Lodger{" +
            "lodgerId=" + lodgerId +
            ", lodgerAccount='" + lodgerAccount + '\'' +
            ", lodgerPassword='" + lodgerPassword + '\'' +
            ", lodgerNickname='" + lodgerNickname + '\'' +
            ", lodgerName='" + lodgerName + '\'' +
            ", lodgerRegisterTime=" + lodgerRegisterTime +
            ", lodgerPhoneNo='" + lodgerPhoneNo + '\'' +
            ", lodgerIdCardNo='" + lodgerIdCardNo + '\'' +
            ", lodgerEmail='" + lodgerEmail + '\'' +
            ", lodgerAddress='" + lodgerAddress + '\'' +
            '}';
  }

  public Long getUserStateId() {
    return userStateId;
  }

  public void setUserStateId(Long userStateId) {
    this.userStateId = userStateId;
  }
}
