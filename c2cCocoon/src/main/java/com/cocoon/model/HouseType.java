package com.cocoon.model;

public class HouseType {
  private Long houseTypeId;
  private String houseTypeName;


  public Long getHouseTypeId() {
    return houseTypeId;
  }

  public void setHouseTypeId(Long houseTypeId) {
    this.houseTypeId = houseTypeId;
  }

  public String getHouseTypeName() {
    return houseTypeName;
  }

  public void setHouseTypeName(String houseTypeName) {
    this.houseTypeName = houseTypeName;
  }
}
